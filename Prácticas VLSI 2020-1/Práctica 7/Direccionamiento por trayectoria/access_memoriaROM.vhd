
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity access_memoriaROM is
Port ( mclk: in  STD_LOGIC;
		 sw:	in  STD_LOGIC_VECTOR(2 downto 0);
       LEDS:	out  STD_LOGIC_VECTOR(5 downto 0));
end access_memoriaROM;

architecture Behavioral of access_memoriaROM is
signal data : STD_LOGIC_VECTOR(8 downto 0);
signal edo_pre: STD_LOGIC_VECTOR(2 downto 0) := "000"; -- se inicializa el estado inicial
signal delay: integer range 0 to 320000000:=0; -- reloj base 320 MHz
signal div: std_logic:='0';

begin
 
edos_salids: entity work.memoriaROM
Port map (addr=> edo_pre & sw, data=>data);--Mapeamos los puertos de los dos módulos
-- Se pasa el estado presente y las entradas como la dirección de la memoria

-- Divisor para la entrada t con periodo de diez segundos
divisor: process(mclk) -- frec divisor = Reloj base(mclk) / N => N= (64MHz / .1 Hz)/2 -1 = 319999999
	begin
		if rising_edge(mclk) then
			if(delay=319999999) then -- el limite de cuenta para el DIVISOR es N
				delay<=0; -- se reinicia el conteo
				div <= not(div);
			else
				delay<=delay+1;
			end if;
		end if;
end process;

-- Cada diez segundos se asignara el estado presente
process(div)
	begin	
		if (rising_edge(div)) then
			edo_pre <= data(8) & data(7) & data(6);	
		end if;
end process;

-- Las salidas se niegan, ya que nuestra tarjeta funciona con lógica negativa
LEDS <= not(data(0) & data(1) & data(2) & data(3) & data(4) & data(5));

end Behavioral;


