=========================================================================
Advanced HDL Synthesis Report

Macro Statistics
# RAMs                                                 : 1
 40x9-bit single-port distributed Read Only RAM        : 1
# Counters                                             : 1
 29-bit up counter                                     : 1
# Registers                                            : 4
 Flip-Flops                                            : 4

=========================================================================