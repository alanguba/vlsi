Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:

Simula el comportamiento de dos bombas A, B. Al comienzo cuando un tanque esté vacío se empezará a llenar con la bomba A, 
cuando si llene se apaga la bomba, si se vuelve a vaciar ahora se llenará con la bomba B, así sucesivamente, 
ciclando el llenado entre la bomba A y B.


El programa mealy.vhd se encuentra la funcionalidad hecha con una FSM de Mealy.

El programa bombas.vhd se encuentra la funcionalidad hecha con una FSM de Moore.


Video:

https://www.youtube.com/watch?v=LOIU5PHzXYk
