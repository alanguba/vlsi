library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mealy is
    Port ( 	mclk: in  STD_LOGIC;		-- reloj
				H: in  STD_LOGIC;			-- sensor de entrada
				A, B: out  STD_LOGIC);	-- bombas de salida
end mealy;

architecture Behavioral of mealy is

	type estado is (E0, E1, E2, E3); 	-- Codificacion de estados
	signal edo_pre, edo_sig:  estado; 	-- Se�ales de tipo estado 
	
begin

	process(mclk)
	begin	
		if (rising_edge(mclk)) then
			edo_pre <= edo_sig;				-- Con el flanco de subida el estado siguiente pasa a ser el presente
		end if;
	end process;
	
	process(edo_pre, H)						-- Proceso para asignar valor a las salidas y cambiar de estado segun las entradas y el estado presente  
	begin
		case edo_pre is
			when E0 =>
				if (H = '1') then
					A<=not('0');				-- Se asigna la salida con logica negada 
					B<=not('0');				-- Se asigna la salida con logica negada 
					edo_sig <= E0;				-- E0 se convierte en el estado siguiente 
				else 
					A<=not('0');				-- Se asigna la salida con logica negada 
					B<=not('0');				-- Se asigna la salida con logica negada 
					edo_sig <= E1;				-- E1 se convierte en el estado siguiente
				end if;
			when E1 =>
				if (H = '1') then
					A<=not('1');				-- Se asigna la salida con logica negada 
					B<=not('0');				-- Se asigna la salida con logica negada 
					edo_sig <= E2;				-- E2 se convierte en el estado siguiente
				else 
					A<=not('1');				-- Se asigna la salida con logica negada 
					B<=not('0');				-- Se asigna la salida con logica negada 
					edo_sig <= E1;				-- E1 se convierte en el estado siguiente
				end if;
			when E2 =>
				if (H = '1') then
					A<=not('0');				-- Se asigna la salida con logica negada 
					B<=not('0');				-- Se asigna la salida con logica negada 
					edo_sig <= E2;				-- E2 se convierte en el estado siguiente
				else 
					A<=not('0');				-- Se asigna la salida con logica negada 
					B<=not('0');				-- Se asigna la salida con logica negada 
					edo_sig <= E3;				-- E3 se convierte en el estado siguiente
				end if;
			when E3 =>
				if (H = '1') then
					A<=not('0');				-- Se asigna la salida con logica negada 
					B<=not('1');				-- Se asigna la salida con logica negada 
					edo_sig <= E0;				-- E0 se convierte en el estado siguiente
				else 
					A<=not('0');				-- Se asigna la salida con logica negada 
					B<=not('1');				-- Se asigna la salida con logica negada 
					edo_sig <= E3;				-- E3 se convierte en el estado siguiente
				end if;
			end case;
	end process;

end Behavioral;

