Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:

Se desea hacer el funcionamiento del protocolo UART, para poder comunicar por medio del puerto serial una computadora
y la FPGA.

Se enviarán números por el puerto serial a la tarjeta FPGA (recepción) y ésta tendrá que mostrar en unos leds su representación binaria.

Se enviará la representación binaria de números de la tarjeta FPGA a la computadora (transmisión) y en la computadora se tendrá que 
mostrar su representación en decimal.

Video:

https://youtu.be/IpoY_NRUKSs