library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Pract8 is
    Port ( CLK : in  STD_LOGIC;
			  RX : in STD_LOGIC;
			  TX : out STD_LOGIC;
			  LED : out STD_LOGIC_VECTOR(7 downto 0):="11111111";
			  SWITCH : in STD_LOGIC_VECTOR(7 downto 0);
			  ENVI : in STD_LOGIC);
end Pract8;

architecture Behavioral of Pract8 is

	component RS232 is 
	generic (FPGA_CLK: integer := 64000000;
				BAUD_RS232: integer := 9600);
	port	(	CLK : 		in std_logic ;			--Reloj de FPGA
				RX :			in std_logic ;			--Pin de recepci�n de RS232
				TX_INI :		in std_logic ;			--Debe ponerse a '1' para inciar transmisi�n
				TX_FIN :		out std_logic ;		--Se pone '1' cuando termina la transmisi�n
				TX :			out std_logic ;		--Pin de transmisi�n de RS232
				RX_IN :		out std_logic ;		--Se pone a '1' cuando se ha recibido un Byte. Solo dura un 
															--Ciclo de reloj
				DATAIN :		in std_logic_vector(7 downto 0); --Puerto de datos de entrada para transmisi�n
				DOUT :		out std_logic_vector(7 downto 0) --Puerto de datos de salida para recepci�n
			);
	end component RS232;
	
	signal tx_ini_signal : std_logic := '1';
	signal tx_fin_signal : std_logic := '0';
	signal rx_in_signal : std_logic := '0';
	signal datain_signal : std_logic_vector(7 downto 0) := X"00";
	signal dout_signal : std_logic_vector(7 downto 0) := X"00";
	signal midato_s : std_logic_vector(7 downto 0) := X"00";
begin

	datain_signal <= SWITCH;
	
	-- Se hace el mapeo de componente
	Inst_RS232: RS232 GENERIC MAP(
		FPGA_CLK => 64000000,
		BAUD_RS232 => 9600)
	PORT MAP(
		CLK => CLK,
		RX => RX,
		TX_INI => tx_ini_signal,
		TX_FIN => tx_fin_signal,
		TX => TX,
		RX_IN => rx_in_signal,
		DATAIN => datain_signal,
		DOUT => dout_signal
	);

	process(CLK,tx_fin_signal,rx_in_signal,ENVI)
	begin
		if(tx_fin_signal = '1') then
			tx_ini_signal <= '0';
		elsif (ENVI = '1') then
			tx_ini_signal <='1'; -- Se pone en 0 para que empiece a mandar la se�al
		end if;
		if(rx_in_signal = '1') then
			midato_s <= dout_signal;
			LED <= not(midato_s - X"30"); -- Se resta 30 en hexadecimal para que aparezca en binario en los leds
		end if;
	end process;

end Behavioral;

