
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity memoriaRom is
	generic(
		--Descripci�n de la esctructura l�gica de la memoria ROM
		addr_width: integer:=16; --N�mero de localidades
		addr_bits: integer:=4;
		data_width: integer:=7);
		--Descripci�n de la esctructura f�sica de la memoria ROM
	Port ( addr:	in  STD_LOGIC_VECTOR(addr_bits - 1 downto 0);
	       data:	out  STD_LOGIC_VECTOR(data_width - 1 downto 0));
end memoriaRom;

architecture Behavioral of memoriaRom is
type rom_type is array(0 to addr_width - 1) of
STD_LOGIC_VECTOR(data_width - 1 downto 0);
signal seg7: rom_type := (
					"0111110", -- V
					"0001110", -- L
					"1011011", -- S
					"0000110", -- I
					"0000001", -- -
					"1101101", -- 2
					"1111110", -- 0
					"1101101", -- 2
					"1111110", -- 0
					"0001000", -- _
					"0000110", -- 1
					"0000001", -- -
					"0111110", -- U
					"1110111", -- A
					"0111110", -- U
					"1000111");-- F
					
begin
data <= seg7(conv_integer(unsigned(addr)));
end Behavioral;

