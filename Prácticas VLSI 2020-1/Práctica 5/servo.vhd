-- El rango del servomotor es de 1ms
-- La frecuencia necesaria se calcula como fnecesaria= (rango/resoluci�n)-1
-- En donde la resoluci�n son las 16 posiciones que puede tomar debido a que se usar�n un 4 switchs para los 180�
-- fnecesaria= (1ms/16) = 16kHz la cual se obtendr� en el primer divisor
-- si se tiene un divisor para una freuencia de 16kHz, entonces para tener una freq de 20 ms, que es la requerida para el servomotor:
-- fservo= 20ms
-- con 16(fnecesaria'-1)= 1ms --cada 16 iteraciones ser�a 1ms
-- para 20 ms:
-- 16*20 = 320/2 = 160 la cual se obtendr� en el segundo divisor

--El divisor debe ir de 0 a 160 para que vaya a la frecuencia requerida por el servomotor.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity servo is
    Port ( mclk	: in  STD_LOGIC;
			switch:	in  STD_LOGIC_VECTOR(3 downto 0);
           servomotor: 		out STD_LOGIC);
end servo;

architecture Behavioral of servo is

signal delay: integer range 0 to 64000000:=0; -- Reloj maestro de 64MHz
signal div: std_logic:='0';

signal contador: unsigned (7 downto 0);
signal pwmi: unsigned (4 downto 0);

begin

divisor: process(mclk) --proceso para divisor de frecuencia
begin
	if rising_edge(mclk) then
		if(delay=3999) then	-- 64 MHz/ fnecesaria => 64M/16k = 4000
			delay<=0;
			div<=not(div);
		else 
			delay<=delay+1;
		end if;
	end if;
end process;

pwmi <= unsigned('0' & switch)+4; -- se suma al n�mero introducido por el switch un offset de 0.5ms 

counter: process (div) begin
	if rising_edge(div) then
		if (contador = 160) then	-- para tener una se�al con frecuencia de 20 ms
			 contador <= (others => '0');
		else
			 contador <= contador + 1;
		end if;
	end if;
end process;

-- Salida para el servomotor
servomotor <= '1' when (contador < pwmi) else '0';

end Behavioral;