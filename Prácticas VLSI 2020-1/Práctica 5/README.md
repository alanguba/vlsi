Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:

Se desea diseñar la descripción de hardware para que se observe el ciclo de trabajo (PWM) en un led, 
de tal forma que varíe la intensidad de éste.

Se desea diseñar la descripción de hardware para que se observe el funcionamiento de un servomotor,  
controlándolo mediante la variación del ciclo de trabajo (PWM).


Video:

https://youtu.be/YON0g5uwgGI
